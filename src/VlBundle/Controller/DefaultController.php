<?php

namespace VlBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use VlBundle\Entity\Event;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {

        /** @var $siteManager \VlBundle\Site\SiteManager */
        $siteManager = $this->container->get('site_manager');
        $domain = $siteManager->getCurrentSite();

        $form = $this->createFormBuilder(new Event(), array(
            'data_class' => 'VlBundle\Entity\Event',
        ))->add('name', 'text')
            ->add('surname', 'text')
            ->add('code', 'text')
            ->add('email', 'email',array('required' => true))
            ->add('marketingAgreement', 'checkbox',array('required' => false))
            ->add('sub','submit',array('label' => 'Register','attr' => array('class' => 'btn btn-lg btn-primary btn-block')))
            ->getForm();

        $status = null;
        if ($request->isMethod('POST')) {
            $form->bind($request);
            $verifyHost = $this->container->getParameter('verify_host');
            $em = $this->getDoctrine()->getManager();

            if ($form->isValid()) {
                $data = $form->getData();

                $find = $em->getRepository('VlBundle:Codes')->findOneByCode($data->getCode());
                if ($find) {

                    $event = new Event();
                    $event->setName($data->getName());
                    $event->setSurname($data->getSurName());
                    $event->setCode($data->getCode());
                    $event->setEmail($data->getEmail());
                    $event->setMarketingAgreement($data->getMarketingAgreement());
                    $event->setRegDate(new \DateTime());


                    $em->persist($event);
                    $find->setStatus(1);
                    $find->setUpdated(new \DateTime());
                    $em->persist($find);
                    $em->flush();


                    return $this->redirect($this->generateUrl('verify', array('code' => $event->getCode())));

                }else{
                    $status = "Invalid code";
                }
            }
        }


        return $this->render('VlBundle:Default:index.html.twig', array(
            'site' => $siteManager->getCurrentSite(), 'form' => $form->createView(), 'status' => $status
        ));
    }

    public function verifyAction(Request $request)
        {


            $code = $request->get('code',null);

            $exists = $this->getDoctrine()
                    ->getRepository('VlBundle:Codes')
                    ->findOneByCodeJoinedToEvent($code);


            if($exists){
                $event = $exists[0];
                $relatedMovies = $this->getDoctrine()
                                    ->getRepository('VlBundle:Codes')
                                    ->findRandomMovieByCode(strtolower($event['code']));

                $randomMovieKey = array_rand($relatedMovies, 1);
                $randomMovie = null;
                if(count($relatedMovies) > 0){
                $randomMovie = $relatedMovies[$randomMovieKey];
                    return $this->render('VlBundle:Default:verify.html.twig', array('data' => $randomMovie));
                }else{
                    return $this->render('VlBundle:Default:notexisted.html.twig', array());
                }




            }else{
                return $this->render('VlBundle:Default:notexisted.html.twig', array());
            }


        }

    public function parameterTestAction($_route)
    {
        return new Response(sprintf('We\'re using parameters! Matched "%s"!', $_route));
    }

    public function randomstring($length = 5)
        {
            $chars = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";
            $i = 0;
            $string = "";
            while ($i <= $length) {
                $string .= $chars{mt_rand(0, strlen($chars) - 1)};
                $i++;
            }
            return $string;
        }
}
