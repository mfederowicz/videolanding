<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace VlBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery as ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;

class CodesAdminController extends Controller
{

    public function editAction($id = null)
        {
            // the key used to lookup the template
            $templateKey = 'edit';

            $id = $this->get('request')->get($this->admin->getIdParameter());
            $object = $this->admin->getObject($id);

            if (!$object) {
                throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
            }

            if (false === $this->admin->isGranted('EDIT', $object)) {
                throw new AccessDeniedException();
            }

            $this->admin->setSubject($object);

            /** @var $form \Symfony\Component\Form\Form */
            $form = $this->admin->getForm();
            $form->setData($object);


            if ($this->getRestMethod() == 'POST') {
                $form->submit($this->get('request'));

                $isFormValid = $form->isValid();

                // persist if the form was valid and if in preview mode the preview was approved
                if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {

                    try {
                        $object = $this->admin->update($object);

                        if ($this->isXmlHttpRequest()) {
                            return $this->renderJson(array(
                                'result'    => 'ok',
                                'objectId'  => $this->admin->getNormalizedIdentifier($object)
                            ));
                        }

                        $this->addFlash(
                            'sonata_flash_success',
                            $this->admin->trans(
                                'flash_edit_success',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );

                        // redirect to edit mode
                        return $this->redirectTo($object);

                    } catch (ModelManagerException $e) {
                        $this->logModelManagerException($e);

                        $isFormValid = false;
                    }
                }

                // show an error message if the form failed validation
                if (!$isFormValid) {
                    if (!$this->isXmlHttpRequest()) {
                        $this->addFlash(
                            'sonata_flash_error',
                            $this->admin->trans(
                                'flash_edit_error',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );
                    }
                } elseif ($this->isPreviewRequested()) {
                    // enable the preview template if the form was valid and preview was requested
                    $templateKey = 'preview';
                    $this->admin->getShow();
                }
            }

            $view = $form->createView();

            // set the theme for the current Admin Form
            $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

            $code_movies = $this->getDoctrine()->getRepository('VlBundle:Codes')
                ->findRandomMovieByCode(strtolower($object->getCode()));

            return $this->render($this->admin->getTemplate($templateKey), array(
                'action' => 'edit',
                'form'   => $view,
                'code_movies'   => $code_movies,
                'all_movies'   => $this->getDoctrine()->getRepository('VlBundle:Movies')->getAllActiveMovies(),
                'object' => $object,
            ));
        }

    /**
         * Create action
         *
         * @return Response
         *
         * @throws AccessDeniedException If access is not granted
         */
        public function createAction()
        {
            // the key used to lookup the template
            $templateKey = 'edit';

            if (false === $this->admin->isGranted('CREATE')) {
                throw new AccessDeniedException();
            }

            $object = $this->admin->getNewInstance();

            $this->admin->setSubject($object);

            /** @var $form \Symfony\Component\Form\Form */
            $form = $this->admin->getForm();
            $form->setData($object);

            if ($this->getRestMethod()== 'POST') {
                $form->submit($this->get('request'));

                $isFormValid = $form->isValid();

                // persist if the form was valid and if in preview mode the preview was approved
                if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {

                    if (false === $this->admin->isGranted('CREATE', $object)) {
                        throw new AccessDeniedException();
                    }

                    try {
                        $object = $this->admin->create($object);

                        if ($this->isXmlHttpRequest()) {
                            return $this->renderJson(array(
                                'result' => 'ok',
                                'objectId' => $this->admin->getNormalizedIdentifier($object)
                            ));
                        }

                        $this->addFlash(
                            'sonata_flash_success',
                            $this->admin->trans(
                                'flash_create_success',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );

                        // redirect to edit mode
                        return $this->redirectTo($object);

                    } catch (ModelManagerException $e) {
                        $this->logModelManagerException($e);

                        $isFormValid = false;
                    }
                }

                // show an error message if the form failed validation
                if (!$isFormValid) {
                    if (!$this->isXmlHttpRequest()) {
                        $this->addFlash(
                            'sonata_flash_error',
                            $this->admin->trans(
                                'flash_create_error',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );
                    }
                } elseif ($this->isPreviewRequested()) {
                    // pick the preview template if the form was valid and preview was requested
                    $templateKey = 'preview';
                    $this->admin->getShow();
                }
            }

            $view = $form->createView();

            // set the theme for the current Admin Form
            $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

            return $this->render($this->admin->getTemplate($templateKey), array(
                'action' => 'create',
                'form'   => $view,
                'code_movies'   => array(),
                'all_movies'   => array(),
                'object' => $object,
            ));
        }
    public function removemovieAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $find = $em->getRepository('VlBundle:CodesHasMovies')->find($id);
        if (!$find) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $find)) {
            throw new AccessDeniedException();
        }

        $back_id = $find->getCode()->getId();
        try {
            $this->admin->delete($find);


        } catch (ModelManagerException $e) {
            $this->logModelManagerException($e);

            if ($this->isXmlHttpRequest()) {
                return $this->renderJson(array('result' => 'error'));
            }


        }


        $this->addFlash('sonata_flash_success', 'flash_delete_success');
        return new RedirectResponse($this->admin->generateUrl(
            'edit',
            array('id' => $find->getCode()->getId())
        ));


        /*$remove = $this->getDoctrine()->getRepository('VlBundle:CodesHasMovies')
                        ->findRandomMovieByCode($id);

        if(!$remove){

        }*/

    }

    private function logModelManagerException($e)
        {
            $context = array('exception' => $e);
            if ($e->getPrevious()) {
                $context['previous_exception_message'] = $e->getPrevious()->getMessage();
            }
            $this->getLogger()->error($e->getMessage(), $context);
        }






}