<?php
namespace VlBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VlBundle\Entity\Codes;

class GenerateCodeCommand extends ContainerAwareCommand {

protected function configure() {

$this->setName('generate:code')
->setDescription('Generate new code')
->addArgument('code', InputArgument::REQUIRED, 'What application database you want to import? prayer');
}

protected function execute(InputInterface $input, OutputInterface $output) {
$code = $input->getArgument('code');
    $code = strtolower($code);
    $repo = $this->getContainer()->get('doctrine')->getRepository('VlBundle:Codes');
    $create = $repo->insertNewCode($code);


}
}