<?php
namespace VlBundle\Admin;

use VlBundle\Admin\CodesAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class MoviesCodesAdmin extends Admin
{
    protected $baseRouteName = 'sonata_movies_codes';
    protected $baseRoutePattern = 'movies_codes';
    protected function configureFormFields(FormMapper $formMapper)
        {

            $formMapper
                ->add('movie', 'integer', array('label' => 'Code'))
                ->add('code', 'text', array('label' => 'Code'));



        }

    // Fields to be shown on lists
        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->addIdentifier('id')
                ->add('movie')
                ->add('code')

            ;
        }
}
