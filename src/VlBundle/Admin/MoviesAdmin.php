<?php
namespace VlBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class MoviesAdmin extends Admin
{
    protected $baseRouteName = 'sonata_movies';
    protected $baseRoutePattern = 'movies';

    protected function configureFormFields(FormMapper $formMapper)
        {

            $formMapper
                ->add('movie')
                ->add('visible','choice', [
                                            'multiple' => false,
                                            'choices' => ['0' => 'no', '1' => 'yes']
                                        ])
                ->add('created', 'sonata_type_datetime_picker',array('required' => false, 'format' => 'yyyy-MM-dd HH:mm:ss'))
                ;



        }

        // Fields to be shown on filter forms
        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $datagridMapper
                ->add('movie')
                ->add('created')
            ;
        }

        // Fields to be shown on lists
        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->addIdentifier('movie')
                ->add('visible')
                ->add('created')
            ;
        }


}
