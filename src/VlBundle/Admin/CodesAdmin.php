<?php
namespace VlBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VlBundle\Entity\CodesHasMovies;

class CodesAdmin extends Admin
{
    protected $baseRouteName = 'sonata_codes';
    protected $baseRoutePattern = 'codes';
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('code', 'text', array('label' => 'Code'))
            ->add('status','choice', [
                        'multiple' => false,
                        'choices' => ['0' => 'new', '1' => 'used']
                    ])


        ;



    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('code')
            ->add('created');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('code')
            ->add('status')
            ->add('created')
            ->add('updated')
        ;
    }

    public function getTemplate($name)
        {
            switch ($name) {
                case 'edit':
                    return 'VlBundle:CodesAdmin:edit.html.twig';
                    break;

                default:
                    return parent::getTemplate($name);
                    break;
            }
        }




    protected function configureRoutes(RouteCollection $collection)
            {
                $collection->add('removemovie', $this->getRouterIdParameter().'/removemovie');
            }


    public function postUpdate($code)
        {
            $codeId = $this->getRequest()->get('id');
            $movieId = $this->getRequest()->get('movie');
            if ($codeId && $movieId) {
                $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
                $find = $em->getRepository('VlBundle:CodesHasMovies')
                    ->findBy(array('code' => $codeId, 'movie' => $movieId));


                if (count($find) == 0) {

                    $chm = new CodesHasMovies();
                    $chm->setCode($codeId);
                    $chm->setMovie($movieId);
                    $chm->setPosition(rand(0, 100));
                    $save = $em->getRepository('VlBundle:CodesHasMovies')->saveNewCHM($chm);

                }
            }



        }


}
