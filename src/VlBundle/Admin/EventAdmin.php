<?php
namespace VlBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class EventAdmin extends Admin
{
    protected $baseRouteName = 'sonata_event';
    protected $baseRoutePattern = 'event';

    protected function configureRoutes(RouteCollection $collection)
        {
            $collection
                ->remove('create');

        }
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('surname')
            ->add('code')
            ->add('email')
            ->add('marketingAgreement','checkbox',array('required' => false))
            ->add('regDate', 'sonata_type_datetime_picker',array('required' => false, 'format' => 'yyyy-MM-dd HH:mm:ss'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('regDate')
            ->add('marketingAgreement')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('surname')
            ->add('code')
            ->add('regDate')
            ->add('marketingAgreement')
            ->add('email')
        ;
    }
}
