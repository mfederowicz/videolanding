<?php
namespace VlBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * CodesHasMovies
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VlBundle\Entity\CodesHasMoviesRepository")
 */
class CodesHasMovies
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="VlBundle\Entity\Codes")
     * @ORM\JoinColumn(name="code_id", referencedColumnName="id",nullable=false,)
     */
    private $code;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="VlBundle\Entity\Movies" ,inversedBy="movies", fetch="LAZY" )
     * @ORM\JoinColumn(name="movie_id", referencedColumnName="id",nullable=false)
     */
    protected $movie;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $movie
     */
    public function setMovie($movie)
    {
        $this->movie = $movie;
    }

    /**
     * @return int
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }




}