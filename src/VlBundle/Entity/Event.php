<?php

namespace VlBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
//use VlBundle\Validator\UniqueEventDate;
//@UniqueEventDate()

/**
 * Event
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VlBundle\Entity\EventRepository")
 * @UniqueEntity(fields="code", message="Code is already registered try another one")
 */
class Event
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private $surname;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;
    /**
     * @var boolean
     *
     * @ORM\Column(name="marketingAgreement", type="boolean")
     */
    private $marketingAgreement;
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @ORM\OneToOne(targetEntity="Code")
     * @ORM\JoinColumn(name="code", referencedColumnName="code")
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="regDate", type="datetime")
     * @Assert\NotBlank
     */
    private $regDate;

    public function __construct()
    {
        $this->setRegDate(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }




    /**
     * @param \DateTime $regDate
     */
    public function setRegDate($regDate)
    {
        $this->regDate = $regDate;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->regDate;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param boolean $marketingAgreement
     */
    public function setMarketingAgreement($marketingAgreement)
    {
        $this->marketingAgreement = $marketingAgreement;
    }

    /**
     * @return boolean
     */
    public function getMarketingAgreement()
    {
        return $this->marketingAgreement;
    }



}
