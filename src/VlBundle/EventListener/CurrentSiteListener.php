<?php

namespace VlBundle\EventListener;

use VlBundle\Site\SiteManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CurrentSiteListener
{
    private $siteManager;

    private $em;

    private $baseHosts;

    public function __construct(SiteManager $siteManager, EntityManager $em, $baseHosts)
    {
        $this->siteManager = $siteManager;
        $this->em = $em;
        $this->baseHosts = $baseHosts;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $host = $request->getHost();
        $baseHosts = $this->baseHosts;
        // if we're at some totally foreign domain, do nothing
        // this prevents all the other demos int his project from failing :)
        if (in_array($host,$baseHosts) === false)  {
            throw new NotFoundHttpException(sprintf('Cannot find site for host "%s"',$host));
        }

        $site = array_search($host,$baseHosts);
        if(!isset($baseHosts[$site])){
            throw new NotFoundHttpException(sprintf('Cannot find site for host "%s"',$host));
        }else{
        $site = $baseHosts[$site];
        }
        $siteManager = $this->siteManager;
        $siteManager->setCurrentSite($site);
    }
}